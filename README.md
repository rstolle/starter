# Purpose

In this project I want to maintain starters for different kind of java projects. 

# An (almost) empty git project

For the first commit, I created a new public project on [gitlab.com](https://gitlab.com/rstolle/starter). I got offered three options:

### (1) Create a new repository

```bash
git clone https://gitlab.com/rstolle/starter.git
cd starter
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### (2) Push an existing folder

```bash
cd existing_folder
git init
git remote add origin https://gitlab.com/rstolle/starter.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### (3) Push an existing Git repository

```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/rstolle/starter.git
git push -u origin --all
git push -u origin --tags
```
Even though I created the project without 
> Initialize repository with a README - Allows you to immediately clone this project’s repository. Skip this if you plan to push up an existing repository. 

I could clone it anyway:

```bash
$ git clone https://gitlab.com/rstolle/starter.git
Cloning into 'starter'...
warning: You appear to have cloned an empty repository.
```  
Then I followed roughly _(1) Create a new repository_.

